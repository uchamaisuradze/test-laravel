<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;

class ProductController extends Controller{

    public function postAddProduct(Request $request){

        $result = [
            'status' => 1,
            'text' => 'Product Add Successfully'
        ];


        $rules = [
            'product_name' => 'required',
            'stock_quantity' => 'required|integer|min:0',
            'price' => 'required|numeric|min:0',
        ];

        $data = $request->all();

        $validate = Validator::make($data, $rules);
        $result['status'] = !$validate->fails();

        if(!$result['status']) {
            $result['text'] = '';
            foreach ($validate->errors()->all() as $error) {
                $result['text'] .= $error . '<br />';
            }
        }
        else{
            $id = md5(time().rand());
            $saveModel = [
                '_id' => $id,
                'product_name' => $data['product_name'],
                'quantity' => $data['stock_quantity'],
                'price' => $data['price'],
                'time' => time()
            ];

            $getFile = json_decode(\File::get('products.json'), true);
            $getFile[$id] = $saveModel;

            \File::put('products.json', json_encode($getFile));

        }

        return response()->json($result);
    }

    public function getList(Request $request){

        $getFile = json_decode(\File::get('products.json'), true);
        usort($getFile, [$this, 'compare'] );
        return view('pages.products-list', ['list' => $getFile, 'sum' => 0]);
    }

    public static function sumValue(&$sum, $plus){
        $sum += $plus;
        return $plus;
    }

    private function compare($a, $b){
        return ($a['time']) < ($b['time']);
    }

}



