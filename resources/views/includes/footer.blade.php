<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<script>

    $(document).ready(function(){

       $('#product_form').submit(function(e){
           e.preventDefault();
           e.stopPropagation();
           var url = $(this).attr('action');
           var params = $(this).serialize();
           var method = $(this).attr('method');
           $.ajax({
               url: url,
               data: params,
               method: method,
               error: function(){

               },
               success: function(data){
                   updateProductTable();
               },
               complete: function(){

               }
           });

           return false;
       });

    });

    function updateProductTable(){
        $.ajax({
            url: '/getList',
            data: '',
            method: 'GET',
            error: function(){

            },
            success: function(data){
                $('#product-list').html(data);
            },
            complete: function(){

            }
        });

        return false;
    }

    updateProductTable();

</script>