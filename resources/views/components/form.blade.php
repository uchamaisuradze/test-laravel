<form id="product_form" method="POST" action="{!! URL::route('addProduct') !!}">
    <div class="form-group">
        <label for="product_name">Product Name</label>
        <input type="text" required="required" class="form-control" id="product_name" name="product_name" placeholder="Name of Product">
    </div>
    <div class="form-group">
        <label for="stock_quantity">Quantity in stock</label>
        <input type="number" required="required" class="form-control" id="stock_quantity" name="stock_quantity" placeholder="Enter Number">
    </div>
    <div class="form-group">
        <label for="price">Product Name</label>
        <input type="number" required="required" class="form-control" id="price" name="price" placeholder="Price">
    </div>
    {{ csrf_field() }}
    <button type="submit" class="btn btn-primary">Submit</button>
</form>