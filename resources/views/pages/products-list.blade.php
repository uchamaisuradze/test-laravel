
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Date</th>
        <th scope="col">Product Name</th>
        <th scope="col">Quantity</th>
        <th scope="col">Price</th>
        <th scope="col">Value</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $key => $value)
    <tr>
        <th scope="row">{!! date('Y-m-d H:i:s', $value['time']) !!}</th>
        <td>{!! $value['product_name'] !!}</td>
        <td>{!! $value['quantity'] !!}</td>
        <td>{!! $value['price'] !!}</td>
        <td>{!! \App\Http\Controllers\ProductController::sumValue($sum, $value['price']*$value['quantity']) !!}</td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
        <th colspan="3"></th>
        <th colspan="1">Sum Value: </th>
        <th>{!! $sum !!}</th>
    </tfoot>
</table>