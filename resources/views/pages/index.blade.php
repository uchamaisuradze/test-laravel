@extends('layouts.master')

@section('title') Test Laravel @endsection

@section('meta-description')  @endsection

@section('main-content')

    <div class="row">

        <div class="col-sm-8">

            @include('components.form')

        </div>

    </div>

    <hr />

    <div class="row">

        <div class="col-sm-12" id="product-list">



        </div>

    </div>

@endsection

@section('foot-content')

@endsection