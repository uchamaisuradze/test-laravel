<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @include('includes.head')

</head>

<body >


<div class="container">

    <div class="row justify-content-md-center">

        <div class="col-sm-12">

            @include('includes.header')


            @yield('main-content')


            @include('includes.footer')

        </div>

    </div>

</div>


</body>

</html>